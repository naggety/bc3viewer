// SPDX-License-Identifier: GPL-2.0-or-later

use crate::reader::{RegsReader, RawReg, RawField, Info, IntoInfo};
use std::collections::HashMap;
use std::error::Error;
use log::{log, error, debug};

// TO-DO: remove pub from fields
pub struct Budget {
    pub root: Chapter,
    pub chapters: Vec<Chapter>,
    pub work_units: Vec<WorkUnit>,
    pub relationships: HashMap<String, Vec<String>>,
    pub texts: HashMap<String, String>,
}

#[derive(Debug)]
pub struct Chapter {
    code: String,
    description: String,
}

pub struct WorkUnit {
    code: String,
    description: String,
}

#[derive(Debug)]
pub struct ParserError {
    msg: String,
    kind: ParserErrorKind,
}

#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum ParserErrorKind {
    IO, Parse
}

type RegParseResult = Result<(), RegParseError>;

#[derive(Debug)]
enum RegParseError {
    Fatal(String), NonFatal(log::Level, String)
}

impl Budget {
    fn new() -> Self {
        Budget {
            root: Chapter {code: String::new(), description: String::new() },
            chapters: Vec::new(),
            work_units: Vec::new(),
            relationships: HashMap::new(),
            texts: HashMap::new()
        }
    }

    pub fn parse(reader: RegsReader) -> Result<Budget, ParserError> {
        let mut parser = Budget::new();

        for reg in reader {
            let reg = reg?;
            let reg_type = reg.unifield(0).unwrap_or("");

            let result = match reg_type {
                "C" => parser.parse_concept(&reg),
                "D" => parser.parse_decomposition(&reg, true),
                "Y" => parser.parse_decomposition(&reg, false),
                "T" => parser.parse_text(&reg),
                "" => {
                    Err(RegParseError::fatal("Got register with 0 fields"))
                },
                unknown => {
                    debug!("Unknown/unsupported register type {}", unknown);
                    Ok(())
                },
            };

            match result {
                Err(RegParseError::NonFatal(lvl, msg)) => {
                    log!(lvl, "{}", msg);
                },
                Err(fatal_err) => {
                    error!("{}", fatal_err.to_string());
                    return Err(fatal_err.into());
                }
                _ => (),
            }
        }

        Ok(parser)
    }

    fn parse_concept(&mut self, reg: &RawReg) -> RegParseResult {
        let ids = reg.field(1).ok_or(RegParseError::log_error("CODE field empty"))?;

        if let Some(code) = ids[0].strip_suffix("##") {
            self.parse_concept_root(reg, code)?;
        }
        else if let Some(code) = ids[0].strip_suffix("#") {
            self.parse_concept_chapter(reg, code)?;
        }
        else {
            self.parse_concept_work_unit(reg, ids[0])?;
        }

        Ok(())
    }

    fn parse_concept_root(&mut self, reg: &RawReg, code: &str) -> RegParseResult {
        if !self.root.code.is_empty() {
            return Err(RegParseError::fatal("The file contains more than one root concept"));
        }

        self.root = Chapter {
            code: String::from(code),
            description: String::from(reg.unifield(3).unwrap_or(""))
        };

        Ok(())
    }

    fn parse_concept_chapter(&mut self, reg: &RawReg, code: &str) -> RegParseResult {
        self.chapters.push(Chapter {
            code: String::from(code),
            description: String::from(reg.unifield(3).unwrap_or("")),
        });

        Ok(())
    }

    fn parse_concept_work_unit(&mut self, reg: &RawReg, code: &str) -> RegParseResult {
        self.work_units.push(WorkUnit {
            code: String::from(code),
            description: String::from(reg.unifield(3).unwrap_or("")),
        });

        Ok(())
    }

    fn parse_decomposition(&mut self, reg: &RawReg, replace: bool) -> RegParseResult {
        if let Some((field, field_n)) = Self::first_nul_or_info(reg, &[3, 2]) {
            if field_n == 3 {
                self.parse_decomposition_field(reg, field, 4, replace)?;
            } else {
                self.parse_decomposition_field(reg, field, 3, replace)?;
            }
        }

        Ok(())
    }

    fn parse_decomposition_field(
        &mut self,
        reg: &RawReg,
        field: RawField,
        chunks_size: usize,
        replace: bool
    ) -> RegParseResult {
        if let Info::Info(ids) = reg.field_info(1) {
            let parent_id = ids[0].trim_end_matches('#');
            let parent = self.relationships
                            .entry(parent_id.to_string())
                            .or_default();

            if replace {
                parent.clear();
            }

            if field.is_info() {
                let children = field.subfields_chunks(chunks_size)
                                    .filter(|chunk| chunk[0].is_info())
                                    .map(|chunk| chunk[0].to_string());
                parent.extend(children);
            }

            Ok(())
        } else {
            Err(RegParseError::log_error("PARENT_CODE field is empty"))
        }
    }

    fn parse_text(&mut self, reg: &RawReg) -> RegParseResult {
        let id = reg.unifield(1).ok_or(RegParseError::log_error("CODE field empty"))?;

        match reg.field_info(2) {
            Info::Info(text) => {
                self.texts.insert(id.to_string(), text[0].to_string());
            },
            Info::Nul => {
                self.texts.remove(id);
            },
            Info::NoInfo => (),
        };

        Ok(())
    }

    /// get the first field that is NUL or WITH_INFORMATION, trying each one in
    /// the order given in fields_pos. This is useful when the spec says that a
    /// field has to be used if present and, if not, other should be used
    fn first_nul_or_info<'a>(reg: &'a RawReg, fields_pos: &[usize]) -> Option<(RawField<'a>, usize)> {
        for &field_n in fields_pos {
            if let Some(field) = reg.field(field_n) {
                if !field.is_no_info() {
                    return Some((field, field_n));
                }
            }
        }
        None
    }
}

impl Error for ParserError {}

impl ParserError {
    pub fn kind(&self) -> ParserErrorKind {
        self.kind
    }
}

impl std::fmt::Display for ParserError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        self.msg.fmt(f)
    }
}

impl From<std::io::Error> for ParserError {
    fn from(e: std::io::Error) -> Self {
        ParserError {msg: e.to_string(), kind: ParserErrorKind::IO}
    }
}

impl From<RegParseError> for ParserError {
    fn from(e: RegParseError) -> Self {
        ParserError { msg: e.to_string(), kind: ParserErrorKind::Parse }
    }
}

impl Error for RegParseError{}

impl RegParseError {
    fn fatal(msg: &str) -> Self {
        Self::Fatal(msg.to_string())
    }

    fn non_fatal(lvl: log::Level, msg: &str) -> Self {
        Self::NonFatal(lvl, msg.to_string())
    }

    fn log_error(msg: &str) -> Self {
        Self::non_fatal(log::Level::Error, msg)
    }

    fn log_warn(msg: &str) -> Self {
        Self::non_fatal(log::Level::Warn, msg)
    }
}

impl std::fmt::Display for RegParseError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            RegParseError::Fatal(msg) => write!(f, "Fatal({})", msg),
            RegParseError::NonFatal(lvl, msg) => write!(f, "{}({})", lvl, msg),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use super::super::reader::RegsReader;

    fn parse_budget(input: &str) -> Budget {
        Budget::parse(RegsReader::new(input.as_bytes()))
            .expect("Error while parsing")
    }

    fn get_chapters_codes(chapters: &[Chapter]) -> Vec<String> {
        chapters.iter()
            .map(|chapter| chapter.code.clone())
            .collect()
    }

    fn get_work_units_codes(work_units: &[WorkUnit]) -> Vec<String> {
        work_units.iter()
            .map(|work_unit| work_unit.code.clone())
            .collect()
    }

    fn assert_children(input: &str, expects: &[&str]) {
        let rels = parse_budget(input).relationships;
        let children = rels.get("PARENT").unwrap();
        assert_eq!(children, expects, "Expected children {:?}, got {:?}", expects, children);
    }

    #[test]
    fn chapter() {
        let input = "~C|EXAMPLE#|";
        let expects = ["EXAMPLE"];

        let budget = parse_budget(&input);
        let chapters = get_chapters_codes(&budget.chapters);
        assert_eq!(chapters, &expects, "Expected chapters {:?}, got {:?}", expects, chapters);
    }

    #[test]
    fn no_chapter() {
        let input = "~C|EXAMPLE|";

        let budget = parse_budget(&input);
        let chapters = get_chapters_codes(&budget.chapters);
        assert!(chapters.is_empty(), "Expected no chapters, got {:?}", chapters);
    }

    #[test]
    fn chapter_info() {
        let input = "~C|EXAMPLE#||Chapter's description|";
        let chapters = parse_budget(&input).chapters;
        assert_eq!(chapters[0].code, "EXAMPLE", "Expected chapter's code 'EXAMPLE' got '{}'", chapters[0].code);
        assert_eq!(chapters[0].description, "Chapter's description",
                   "Expected chapter's description 'Chapter's description', got '{}'", chapters[0].description);
    }

    #[test]
    fn many_chapters() {
        let input = r"~C|ONE#|
                      ~C|TWO#|
                      ~C|THREE#|
                      ~C|NOT_CHAPTER|
                      ~C|FOUR#|";
        let expects = ["ONE", "TWO", "THREE", "FOUR"];

        let budget = parse_budget(&input);
        let chapters = get_chapters_codes(&budget.chapters);
        assert_eq!(chapters, &expects, "Expected chapters {:?}, got {:?}", expects, chapters);
    }

    #[test]
    fn decomposition() {
        let input = r"~C|PARENT#|
                      ~C|CHILD#|
                      ~D|PARENT||CHILD_1\\\\CHILD_2\\\\|";
        assert_children(input, &["CHILD_1", "CHILD_2"]);
    }

    #[test]
    fn decomposition_legacy() {
        let input = r"~C|PARENT#|
                      ~C|CHILD#|
                      ~D|PARENT|CHILD_1\\\CHILD_2\\\||";
        assert_children(input, &["CHILD_1", "CHILD_2"]);
    }

    #[test]
    fn decomposition_both_fields() {
        let input = r"~C|PARENT#|
                      ~C|CHILD#|
                      ~C|OTHER#|
                      ~D|PARENT|OTHER_1\\\OTHER_2\\\|CHILD_1\\\\CHILD_2\\\\|";
        assert_children(input, &["CHILD_1", "CHILD_2"]);
    }

    #[test]
    fn decomposition_replace() {
        let input = r"~C|PARENT#|
                      ~C|CHILD_1|
                      ~C|CHILD_2|
                      ~D|PARENT||CHILD_1\\\\|
                      ~D|PARENT||CHILD_2\\\\|";
        assert_children(input, &["CHILD_2"]);
    }

    #[test]
    fn decomposition_append() {
        let input = r"~C|PARENT#|
                      ~C|CHILD_1|
                      ~C|CHILD_2|
                      ~D|PARENT||CHILD_1\\\\|
                      ~Y|PARENT||CHILD_2\\\\|";
        assert_children(input, &["CHILD_1", "CHILD_2"]);
    }

    #[test]
    fn root() {
        let input = r"~C|ROOT##|";
        let budget = parse_budget(input);
        assert_eq!(budget.root.code, "ROOT", "Expected root concept 'ROOT', got {}", budget.root.code);
        assert!(budget.chapters.is_empty(), "Expected 0 chapters, got {}", budget.chapters.len());
    }

    #[test]
    fn root_and_chapters() {
        let input = r"~C|CHAPTER_1#|
                      ~C|NOT_CHAPTER_NOR_ROOT|
                      ~C|ROOT##|
                      ~C|CHAPTER_2#|";
        let chapters_expects = ["CHAPTER_1", "CHAPTER_2"];

        let budget = parse_budget(input);
        let chapters = get_chapters_codes(&budget.chapters);
        assert_eq!(budget.root.code, "ROOT", "Expected root concept 'ROOT', got {}", budget.root.code);
        assert_eq!(chapters, &chapters_expects, "Expected chapters {:?}, got {:?}", chapters_expects, chapters);
    }

    #[test]
    fn root_x2_error() {
        let input = r"~C|ROOT_1##|
                      ~C|ROOT_2##|";

        let parse_result = Budget::parse(RegsReader::new(input.as_bytes()));
        match parse_result {
            Ok(_) => panic!("Expected ParserError, got Ok"),
            Err(e) => assert_eq!(e.kind(), ParserErrorKind::Parse,
                                 "Expected {:?}, got {:?}", ParserErrorKind::Parse, e.kind()),
        }
    }

    #[test]
    fn work_unit() {
        let input = r"~C|WORK_UNIT|";
        let expects = ["WORK_UNIT"];

        let budget = parse_budget(&input);
        let work_units = get_work_units_codes(&budget.work_units);
        assert_eq!(work_units, &expects, "Expected work_units {:?}, got {:?}", expects, work_units);
    }

    #[test]
    fn text() {
        let input = r"~T|CONCEPT|Text content|";
        let expects = "Text content";

        let budget = parse_budget(&input);
        assert_eq!(budget.texts.len(), 1, "Expected 1 texts, got {}", budget.texts.len());
        let text = &budget.texts["CONCEPT"];
        assert_eq!(text, expects, "Expected the text '{}', got {}", expects, text);
    }
}
