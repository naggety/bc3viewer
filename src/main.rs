// SPDX-License-Identifier: GPL-2.0-or-later

mod reader;
mod parser;
// mod regs_fmt;

use reader::RegsReader;
use parser::Budget;
use std::fs::File;
use env_logger;

fn main() {
	env_logger::Builder::from_env("LOG").init();

	let regs_reader = RegsReader::new(File::open("test.bc3").unwrap());

	// for (i, reg) in regs_reader.enumerate() {
	// 	println!("Register #{}", i);
	// 	for (j, field) in reg.unwrap().fields().enumerate() {
	// 		println!("    Field #{}: {}", j, field.join(", "));
	// 	}
	// }

	let budget = Budget::parse(regs_reader).unwrap();
	println!("Chapters\n========================\n {:#?}\n\n\n", budget.chapters);
	println!("Relationships\n========================\n {:#?}", budget.relationships);
}
