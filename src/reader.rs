// SPDX-License-Identifier: GPL-2.0-or-later

use std::io::{Read, BufRead, BufReader};
use std::ops::Deref;
use log::{warn, debug};
use encoding_rs::WINDOWS_1252;
use oem_cp::code_table::DECODING_TABLE_CP437;
use oem_cp::code_table::DECODING_TABLE_CP850;
use oem_cp;

/// Reader for a stream with bc3 formatted data, reading register by register
///
/// It will normally be used to read a File, but any type that implements
/// Read can be used, such as &[u8]
///
/// This type is itself an Iterator. Advancing the Iterator reads the next
/// register and yields it as RawReg type, which is the register splitted by
/// fields and subfields.
///
/// It's a one pass Iterator and it doesn't store already read data, letting to
/// the caller to do such if needed.
///
/// If the first register of the file or stream is a ~V register, it will get
/// the text encoding from it. Otherwise, the spec's default MSDOS CP850 will be
/// used.
///
/// ```
/// let file = File::open("test.bc3");
/// let reader = RegsReader::new(file);
///
/// for (i, reg) in reader.enumerate() {
///     println!("Register #{}", i);
///     for field in reg.fields() {
///            println!("  - Field: {}", field.join(", "));
///     }
/// }
/// ```
pub struct RegsReader<'a> {
    iter: Box<dyn Iterator<Item = std::io::Result<Vec<u8>>> + 'a>,
    encoding: Option<Encoding>,
}

/// Register parsed and yielded by RegsReader while iterating the bc3 file or
/// stream
///
/// It contains one bc3 register, splitted by fields and subfields. Each field
/// is a vector of subfields, and each subfield it's just a &str of its raw
/// content from the bc3 register.
///
/// ```
/// let reg = regs_reader.next().unwrap();
///
/// for (i, field) in reg.fields().enumerate() {
///     println!("Field {}", i);
///     for subfield in field {
///         println!("  - Subfield: {}", subfield);
///     }
/// }
/// ```
#[derive(Clone, Debug, PartialEq, Eq)]
pub struct RawReg {
    pub raw: String,  // mark as `pub` so it can be printed in debug messages
    fields: Vec<RawFieldPriv>,
}

/// Content of a register's field: each one containes one or more subfields
#[derive(Clone, Debug, PartialEq, Eq)]
pub struct RawField<'a>(Vec<&'a str>);

/// Content of a field or subfield with basic parsing to differenciate between
/// the 3 basic states of the spec: NUL, NO_INFORMATION and WITH_INFORMATION
#[derive(Clone, Debug, PartialEq, Eq)]
pub enum Info<T> {
    Nul, NoInfo, Info(T)
}

/// Iterator of the fields contained in a RawReg, each item being a RawField
pub struct FieldsIter<'a>(<&'a Vec<RawFieldPriv> as IntoIterator>::IntoIter);

/// Iterator of the fields of a RawReg, each item being an Info<RawField>
pub struct FieldsInfoIter<'a>(<&'a Vec<RawFieldPriv> as IntoIterator>::IntoIter, usize);

/// Iterator of the subfields of a RawField, each item being an Info<&str>
pub struct SubfieldsInfoIter<'a>(<&'a Vec<&'a str> as IntoIterator>::IntoIter);

/// Internal private representation of a field
type RawFieldPriv = Vec<(*const u8, usize)>;

/// Supported file encodings
#[derive(Debug, PartialEq, Clone, Copy)]
enum Encoding {
    CP437, CP850, WIN1252
}

impl<'a> RegsReader<'a> {
    pub fn new<T: Read + 'a>(stream: T) -> Self {
        let file_reader = BufReader::new(stream);
        let iter = file_reader.split(b'~').skip(1);
        Self {
            iter: Box::new(iter),
            encoding: None
        }
    }

    fn next_register(&mut self, buf: Vec<u8>) -> RawReg {
        let encoding = self.encoding.get_or_insert_with(|| Self::select_encoding(&buf));

        let reg = match encoding {
            Encoding::WIN1252 => WINDOWS_1252.decode(&buf).0.to_string(),
            Encoding::CP437 => oem_cp::decode_string_complete_table(&buf, &DECODING_TABLE_CP437),
            Encoding::CP850 => oem_cp::decode_string_complete_table(&buf, &DECODING_TABLE_CP850),
        };

        let mut fields = Self::split_register(&reg);
        fields.pop(); // last element is after the register's closing '|', discard

        debug!("~{}", reg.trim_end());
        RawReg {raw: reg, fields}
    }

    fn split_register(reg: &str) -> Vec<RawFieldPriv> {
        reg.split('|').map(Self::split_field).collect()
    }

    fn split_field(field: &str) -> RawFieldPriv {
        if field.is_empty() {
            return Vec::new();
        }

        field.split('\\')
            .map(|subfield| (subfield as *const str as *const u8, subfield.len()))
            .collect()
    }

    // this function is called only when reading the very first register
    fn select_encoding(buf: &[u8]) -> Encoding {
        if buf[0] == b'V' {
            match Self::get_encoding_from_reg(buf) {
                Ok(encoding) => return encoding,
                Err(err) => warn!("Cannot get file encoding: {}. Using default: MSDOS CP850.", err),
            }
        }
        Encoding::CP850  // default value by spec
    }

    fn get_encoding_from_reg(buf: &[u8]) -> Result<Encoding, String> {
        let enc_field = buf.split(|ch| *ch == b'|')
            .nth(5)
            .ok_or("Missing field CHARACTER_SET")?;

        match std::str::from_utf8(enc_field).unwrap_or("").trim() {
            "437" => Ok(Encoding::CP437),
            "850" | "" => Ok(Encoding::CP850),
            "ANSI" => Ok(Encoding::WIN1252),
            enc => Err(format!("Unknown '{}' value in field CHARACTER_SET", enc).into()),
        }
    }
}

impl<'a> Iterator for RegsReader<'a> {
    type Item = std::io::Result<RawReg>;

    fn next(&mut self) -> Option<Self::Item> {
        match self.iter.next() {
            Some(Ok(next)) => Some(Ok(self.next_register(next))),
            Some(Err(e)) => Some(Err(e)),
            None => None,
        }
    }
}

impl RawReg {
    /// get number of fields
    pub fn len(&self) -> usize {
        return self.fields.len();
    }

    /// get the field at pos n
    pub fn field(&self, n: usize) -> Option<RawField> {
        Some(Self::convert_raw_field(self.fields.get(n)?))
    }

    /// get the field at pos n, as Info (Nul / NoInfo / Info)
    pub fn field_info(&self, n: usize) -> Info<RawField> {
        match self.field(n) {
            Some(field) => field.into_info(),
            None => Info::NoInfo
        }
    }

    /// get the content of a field with only one subfield
    /// it's unchecked: if the field has many subfields, it just returns the first
    pub fn unifield(&self, n: usize) -> Option<&str> {
        self.field(n)?.0.get(0).copied()
    }

    /// iterate fields
    pub fn fields(&self) -> FieldsIter {
        FieldsIter(self.fields.iter())
    }

    /// iterate fields, as Info (Nul / NoInfo / Info)
    pub fn fields_info_iter(&self, fields_num: usize) -> FieldsInfoIter {
        FieldsInfoIter(self.fields.iter(), fields_num)
    }

    fn convert_raw_field(raw_field: &RawFieldPriv) -> RawField {
        RawField(
            raw_field.iter()
                .map(|sf| unsafe{std::str::from_utf8_unchecked(std::slice::from_raw_parts(sf.0, sf.1))})
                .collect()
        )
    }
}

impl<'a> RawField<'a> {
    /// get slice of the subfields
    pub fn as_slice(&self) -> &[&str] {
        self.0.as_slice()
    }

    /// get subfield at pos n
    pub fn subfield(&self, n: usize) -> Option<&str> {
        self.0.get(n).copied()
    }

    /// get subfield at pos n, as Info (Nul / NoInfo / Info)
    pub fn subfield_info(&self, n: usize) -> Option<Info<&str>> {
        self.subfield(n).map(|subfield| Self::sf_info(subfield))
    }

    /// iterate subfields
    pub fn subfields(&self) -> impl Iterator<Item = &str> {
        self.iter().copied()
    }

    /// iterate subfields, grouped in non-overlapping chunks of the given size
    pub fn subfields_chunks(&self, size: usize) -> impl Iterator<Item = &[&str]> {
        self.chunks_exact(size)
    }

    /// iterate subfields, as Info (Nul / NoInfo / Info)
    pub fn subfields_info_iter(&self) -> impl Iterator<Item = Info<&str>> {
        SubfieldsInfoIter(self.iter())
    }

    /// iterate subfields, grouped in non-overlapping chunks of the given size, as Info (Nul / NoInfo / Info)
    pub fn subfields_info_chunks(&self, size: usize) -> impl Iterator<Item = Vec<Info<&str>>> {
        self.chunks_exact(size)
            .map(|subfields| subfields.iter().map(|sf| sf.into_info()).collect())
    }

    fn sf_info(subfield: &str) -> Info<&str> {
        match subfield {
            "NUL" => Info::Nul,
            "" => Info::NoInfo,
            sf => Info::Info(sf),
        }
    }
}

pub trait IntoInfo
where
    Self: Sized
{
    fn is_no_info(&self) -> bool;

    fn is_nul(&self) -> bool;

    fn is_info(&self) -> bool {
        !self.is_no_info() && !self.is_nul()
    }

    fn into_info(self) -> Info<Self> {
        if self.is_no_info() {
            Info::NoInfo
        } else if self.is_nul() {
            Info::Nul
        } else {
            Info::Info(self)
        }
    }
}

impl<'a> IntoInfo for RawField<'a> {
    fn is_no_info(&self) -> bool {
        self.0.len() == 0
    }

    fn is_nul(&self) -> bool {
        self.0.len() == 1 && self.0[0] == "NUL"
    }
}

impl IntoInfo for &str {
    fn is_no_info(&self) -> bool {
        self == &""
    }

    fn is_nul(&self) -> bool {
        self == &"NUL"
    }
}

impl<'a> Deref for RawField<'a> {
    type Target = [&'a str];

    fn deref(&self) -> &Self::Target {
        self.0.as_slice()
    }
}

impl<'a> Iterator for FieldsIter<'a> {
    type Item = RawField<'a>;

    fn next(&mut self) -> Option<Self::Item> {
        Some(RawReg::convert_raw_field(self.0.next()?))
    }
}

impl<'a> Iterator for FieldsInfoIter<'a> {
    type Item = Info<RawField<'a>>;

    fn next(&mut self) -> Option<Self::Item> {
        if let Some(raw_field) = self.0.next() {
            self.1 -= 1;
            let field = RawReg::convert_raw_field(raw_field);
            Some(field.into_info())
        } else if self.1 > 0 {
            self.1 -= 1;
            Some(Info::NoInfo)
        } else {
            None
        }
    }
}

impl<'a> Iterator for SubfieldsInfoIter<'a> {
    type Item = Info<&'a str>;

    fn next(&mut self) -> Option<Self::Item> {
        self.0.next().map(|subfield| RawField::sf_info(subfield))
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use super::Info::*;

    macro_rules! fields {
        ( $($x:tt),* ) => { vec![$( field!($x) ),*] };
    }

    macro_rules! field {
        ( [$($x:expr),*] ) => { RawField(vec![$( $x ),*]) };
        ( $x:expr ) => { RawField(vec![$x]) };
    }

    macro_rules! fields_info {
        ( $($x:tt),* ) => { vec![$( field_info!($x) ),* ] };
    }

    macro_rules! field_info {
        (Nul) => { Nul };
        (NoInfo) => { NoInfo };
        ($x:expr) => { Info(field!($x)) }
    }

    fn read(input: &[u8]) -> Vec<RawReg> {
        let result: std::io::Result<Vec<_>> = RegsReader::new(input).collect();
        result.expect(&format!("Input can't be parsed: {}", &std::str::from_utf8(input).unwrap()))
    }

    fn read_one(input: &[u8]) -> RawReg {
        let mut regs = read(input);
        assert_eq!(regs.len(), 1, "Expected 1 register, got {}", regs.len());
        regs.remove(0)
    }

    fn assert_one_register(input: &[u8], expects: &[RawField]) {
        let reg = read_one(input);
        let fields: Vec<_> = reg.fields().collect();
        assert_eq!(fields, expects, "Expected fields {:?}, got {:?}", expects, fields);
    }

    fn assert_many_registers(input: &[u8], expects: &[Vec<RawField>]) {
        let regs = read(input);
        assert_eq!(regs.len(), expects.len(), "Expected {} registers, got {}", expects.len(), regs.len());

        for i in 0..expects.len() {
            let fields: Vec<_> = regs[i].fields().collect();
            assert_eq!(fields, expects[i], "Expected fields {:?} in reg #{}, got {:?}", expects[i], i, fields);
        }
    }

    fn assert_field_info(input: &[u8], field_n: usize, expects: super::Info<RawField>) {
        let reg = read_one(input);
        let field_info = reg.field_info(field_n);
        assert_eq!(field_info, expects, "Expected field_info {:?} at field #{}, got {:?}", expects, field_n, field_info);
    }

    fn assert_fields_info_iter(input: &[u8], fields_num: usize, expects: &[super::Info<RawField>]) {
        let reg = read_one(input);
        let fields_info: Vec<_> = reg.fields_info_iter(fields_num).collect();
        assert_eq!(fields_info, expects, "Expected fields info {:?}, got {:?}", expects, fields_info);
    }

    fn assert_subfield_info(input: &[u8], subfield_n: usize, expects: Option<super::Info<&str>>) {
        let reg = read_one(input);
        let field = reg.field(1).unwrap();
        let subfield_info = field.subfield_info(subfield_n);
        assert_eq!(subfield_info, expects, "Expected subfield_info {:?} at subfield #{}, got {:?}", expects, subfield_n, subfield_info);
    }

    #[test]
    fn one_register() {
        let input = b"~A|a|b|c|";
        let expects = fields!["A", "a", "b", "c"];
        assert_one_register(input, &expects);
    }

    #[test]
    fn many_registers() {
        let input = b"~A|a|\r\n~B|b|\r\n~C|c|";
        let expects = [fields!["A", "a"], fields!["B", "b"], fields!["C", "c"]];
        assert_many_registers(input, &expects);
    }

    #[test]
    fn many_registers_same_line() {
        let input = b"~A|a|~B|b|~C|c|";
        let expects = [fields!["A", "a"], fields!["B", "b"], fields!["C", "c"]];
        assert_many_registers(input, &expects);
    }

    #[test]
    fn register_multiline() {
        let input = b"~A|a\r\n|\r\nb|c|";
        let expects = fields!["A", "a\r\n", "\r\nb", "c"];
        assert_one_register(input, &expects);
    }

    #[test]
    fn discarded_data() {
        let input = b"DISCARD ONE~A|a|b|DISCARD TWO~B|b| DISCARD \r\n THREE";
        let expects = [fields!["A", "a", "b"], fields!["B", "b"]];
        assert_many_registers(input, &expects);
    }

    #[test]
    fn one_field() {
        let input = b"~A|a|b|";
        let reg = read_one(input);
        assert_eq!(reg.field(0), Some(field!("A")));
        assert_eq!(reg.field(1), Some(field!("a")));
        assert_eq!(reg.field(2), Some(field!("b")));
    }

    #[test]
    fn empty_fields() {
        let input = b"~A|a|||d||f||";
        let expects = fields!["A", "a", [], [], "d", [], "f", []];
        assert_one_register(input, &expects);
    }

    #[test]
    fn one_excess_field() {
        let input = b"~A|a|";
        let reg = read_one(input);
        assert_eq!(reg.field(2), None);
    }

    #[test]
    fn field_info() {
        let input = b"~A|a|";
        let reg = read_one(input);
        assert!(reg.field(1).unwrap().is_info(), "Expected is_info() == true in field 1");
    }

    #[test]
    fn field_info_from_reg() {
        let input = b"~A|a|";
        assert_field_info(input, 1, field_info!("a"));
    }

    #[test]
    fn field_nul() {
        let input = b"~A|NUL|";
        let reg = read_one(input);
        assert!(reg.field(1).unwrap().is_nul(), "Expected is_nul() == true in field 1");
    }

    #[test]
    fn field_nul_from_reg() {
        let input = b"~A|NUL|";
        assert_field_info(input, 1, field_info!(Nul));
    }

    #[test]
    fn field_no_info() {
        let input = b"~A||";
        let reg = read_one(input);
        assert!(reg.field(1).unwrap().is_no_info(), "Expected is_no_info() == true in field 1");
    }

    #[test]
    fn field_no_info_from_reg() {
        let input = b"~A||";
        assert_field_info(input, 1, field_info!(NoInfo));
    }

    #[test]
    fn excess_field_from_reg_is_no_info() {
        let input = b"~A||";
        assert_field_info(input, 2, field_info!(NoInfo));
    }

    #[test]
    fn field_info_iter() {
        let input = b"~A|a|NUL||";
        let expects = fields_info!["A", "a", Nul, NoInfo];
        assert_fields_info_iter(input, 4, &expects);
    }

    #[test]
    fn excess_field_info_iter_is_no_info() {
        let input = b"~A|a|";
        let expects = fields_info!["A", "a", NoInfo];
        assert_fields_info_iter(input, 3, &expects);
    }

    #[test]
    fn subfields() {
        let input = br"~A|a|ba\bb||da\db\dc|";
        let expects = fields!["A", "a", ["ba", "bb"], [], ["da", "db", "dc"]];
        assert_one_register(input, &expects);
    }

    #[test]
    fn one_subfield() {
        let input = br"~A|a\b\c|";
        let reg = read_one(input);
        let field = reg.field(1).unwrap();
        assert_eq!(field.subfield(0), Some("a"));
        assert_eq!(field.subfield(1), Some("b"));
        assert_eq!(field.subfield(2), Some("c"));
    }

    #[test]
    fn one_excess_subfield() {
        let input = br"~A|a\b|";
        let reg = read_one(input);
        let field = reg.field(1).unwrap();
        assert_eq!(field.subfield(2), None);
    }

    #[test]
    fn subfield_info() {
        let input = br"~A|a\b\c|";
        let reg = read_one(input);
        let field = reg.field(1).unwrap();
        assert!(field.subfield(1).unwrap().is_info(), "Expected is_info() == true in subfield 1");
    }

    #[test]
    fn subfield_info_from_field() {
        let input = br"~A|a\b\c|";
        assert_subfield_info(input, 1, Some(Info("b")));
    }

    #[test]
    fn subfield_nul() {
        let input = br"~A|a\NUL\c|";
        let reg = read_one(input);
        let field = reg.field(1).unwrap();
        assert!(field.subfield(1).unwrap().is_nul(), "Expected is_nul() == true in subfield 1")
    }

    #[test]
    fn subfield_nul_from_field() {
        let input = br"~A|a\NUL\c|";
        assert_subfield_info(input, 1, Some(Nul));
    }

    #[test]
    fn subfield_no_info() {
        let input = br"~A|a\\c|";
        let reg = read_one(input);
        let field = reg.field(1).unwrap();
        assert!(field.subfield(1).unwrap().is_no_info(), "Expected is_no_info() == true in subfield 1")
    }

    #[test]
    fn subfield_no_info_from_field() {
        let input = br"~A|a\\c|";
        assert_subfield_info(input, 1, Some(NoInfo));
    }

    #[test]
    fn excess_subfield_info_from_field_is_none() {
        let input = br"~A|a\b\c|";
        assert_subfield_info(input, 3, None);
    }

    #[test]
    fn subfield_info_iter() {
        let input = br"~A|a\NUL\\d|";
        let expects = vec![Info("a"), Nul, NoInfo, Info("d")];

        let reg = read_one(input);
        let field = reg.field(1).unwrap();
        let subfields_info: Vec<_> = field.subfields_info_iter().collect();
        assert_eq!(subfields_info, expects, "Expected subfields {:?}, got {:?}", expects, subfields_info);
    }

    #[test]
    fn subfields_chunks() {
        let input = br"~A|a\b\c\d\e\f|";
        let expects = [vec!["a", "b", "c"], vec!["d", "e", "f"]];

        let reg = read_one(input);
        let field = reg.field(1).unwrap();
        let subfields_chunks: Vec<_> = field.subfields_chunks(3).collect();
        assert_eq!(subfields_chunks, expects, "Expected subfield chunks {:?}, got {:?}", expects, subfields_chunks);
    }

    #[test]
    fn subfields_optional_closing_separator() {
        // subfield closing separator is optional, but here we still don't know how many subfields
        // are expected (it depends on the register type). Here we can only be sure that chunks
        // works fine with or without this closing separator
        let input = br"~A|a\b\|";
        let expects = [vec!["a", "b"]];

        let reg = read_one(input);
        let field = reg.field(1).unwrap();
        let subfields_chunks: Vec<_> = field.subfields_chunks(2).collect();
        assert_eq!(subfields_chunks, expects, "Expected subfield chunks {:?}, got {:?}", expects, subfields_chunks);
    }

    #[test]
    fn spaces() {
        let input = br"~A|reg 1|sf 2.0\sf 2.1\sf 2.2 the last||reg 4|";
        let expects = fields!["A", "reg 1", ["sf 2.0", "sf 2.1", "sf 2.2 the last"], [], "reg 4"];
        assert_one_register(input, &expects);
    }

    fn assert_encoding(input: &[u8], expects: Encoding) {
        let mut reader = RegsReader::new(input);
        let reg = reader.nth(1).unwrap().unwrap();
        assert!(reader.encoding.is_some(), "Expected encoding {:?}, got None", expects);
        let encoding = reader.encoding.unwrap();
        assert_eq!(encoding, expects, "Expected encoding {:?}, got {:?}", expects, encoding);
        assert_eq!(reg.field(1).unwrap()[0], "ñú", "String parsed incorrectly for encoding {:?}", expects);
    }

    #[test]
    fn encodings() {
        let input = b"~V|||||ANSI|~A|\xF1\xFA|";
        assert_encoding(input, Encoding::WIN1252);

        let input = b"~V|||||437|~A|\xA4\xA3|";
        assert_encoding(input, Encoding::CP437);

        let input = b"~V|||||850|~A|\xA4\xA3|";
        assert_encoding(input, Encoding::CP850);
    }

    #[test]
    fn encoding_empty() {
        let input = b"~V||||||~A|\xA4\xA3|";
        assert_encoding(input, Encoding::CP850);
    }

    #[test]
    fn encoding_missing_v_reg() {
        let input = b"~A|a|~A|\xA4\xA3|";
        assert_encoding(input, Encoding::CP850);
    }
}
